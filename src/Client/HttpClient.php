<?php


namespace GoCRM\API\Http\Client;


use GoCRM\API\Http\Exceptions\GoCRMHttpResponseException;
use GoCRM\API\Http\HttpResponse;

class HttpClient
{
    /**
     * @var string
     */
    protected $domain;

    /**
     * @var string|null
     */
    protected $systemKey;

    /**
     * @var string|null
     */
    protected $appToken;

    /**
     * @var string|null
     */
    protected $authToken;

    /**
     * @var bool
     */
    protected $secure = false;

    /**
     * @var string
     */
    protected $version = 'v3';


    public function __construct(string $domain)
    {
        $this->domain = $domain;
    }

    public function setSystemKey(?string $systemKey): void
    {
        $this->systemKey = $systemKey;
    }

    /**
     * @param string|null $appToken
     */
    public function setAppToken(?string $appToken): void
    {
        $this->appToken = $appToken;
    }

    /**
     * @param string|null $authToken
     */
    public function setAuthToken(?string $authToken): void
    {
        $this->authToken = $authToken;
    }

    /**
     * @param bool $secure
     */
    public function setSecure(bool $secure): void
    {
        $this->secure = $secure;
    }

    /**
     * @param string|null $httpMethod
     * @param string|null $url
     * @param array|null $options
     * @return HttpResponse
     * @throws \GoCRM\API\Http\Exceptions\GoCRMHttpResponseException
     */
    public function send(?string $httpMethod = 'GET', ?string $url = '/', ?array $options = []): HttpResponse
    {
        $uri = $this->buildUri($url, $options['props']??null);
        $headers = $this->buildHeaders($options['headers']??null);

        $ch = curl_init($uri);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $httpMethod);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        if ($this->secure) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        }

        if (in_array($httpMethod, array('POST', 'PUT'))) {
            $body = $this->buildBody($options['body']??null);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        }

        if (!$response = curl_exec($ch)) {
            throw new GoCRMHttpResponseException(2);
        }

        return new HttpResponse($response);
    }

    /**
     * @param string|null $url
     * @param array|null $props
     * @return string
     */
    private function buildUri(?string $url = '/', ?array $props = []): string
    {
        $path = self::mergePath('/', $this->domain, 'api', $this->version, $url);
        return 'http' .($this->secure ? 's' : '') .'://' . (empty($props) ? $path : self::mergePath($path,'?', http_build_query($props??[])));
    }

    /**
     * @param array|null $headers
     * @return array
     */
    private function buildHeaders(?array $headers = []): array
    {
        return array_merge($headers??[],
            ['Content-Type: application/json; charset=utf-8;'],
            (!empty($this->systemKey)? ['SYS-KEY:'. $this->systemKey] : []),
            (!empty($this->appToken)? ['APP-TOKEN:'. $this->appToken] : []),
            (!empty($this->authToken)? ['AUTH-TOKEN:'. $this->authToken] : [])
        );
    }

    /**
     * @param array|null $body
     * @return string
     */
    private function buildBody(?array $body = []): string
    {
        return json_encode(array_merge($body??[], []));
    }

    /**
     * @param string $separator
     * @param string ...$parts
     * @return string
     */
    public static function mergePath(string $separator, string ...$parts): string
    {
        $_parts = [];

        foreach ($parts as $part) {
            if (!empty($part))
                $_parts[] = preg_replace('/(^http)|(^https)|(\:\/\/)|(^\/)|(\/$)/', '', trim($part));
        }

        return implode($separator, $_parts);
    }
}
