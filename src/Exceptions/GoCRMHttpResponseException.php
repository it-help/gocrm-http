<?php


namespace GoCRM\API\Http\Exceptions;


class GoCRMHttpResponseException extends \Exception
{
    private CONST ERRORS = [
        0 => 'Unknown error',
        1 => 'The response contains an invalid json string',
        2 => 'The Response invalid json schema'
    ];

    CONST ERROR_UNKNOWN = 0;
    CONST ERROR_INVALID_RESPONSE = 1;
    CONST ERROR_INVALID_SCHEMA = 2;

    /**
     * GoCRMHttpResponseException constructor.
     * @param int|null $errorCode
     */
    public function __construct(?int $errorCode = 0)
    {
        parent::__construct(self::ERRORS[$errorCode], $errorCode);
    }
}
