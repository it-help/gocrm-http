<?php


namespace GoCRM\API\Http;


use GoCRM\API\Http\Exceptions\GoCRMHttpResponseException;

class HttpResponse
{
    /**
     * @var string
     */
    private $rawData;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $message;

    /**
     * @var array
     */
    private $validationErrors;

    /**
     * @var array
     */
    private $data;

    /**
     * @var array
     */
    private $pagination;

    /**
     * HttpResponse constructor.
     * @param $response
     * @throws GoCRMHttpResponseException
     */
    public function __construct($response)
    {
        $this->rawData = $response;
        $this->parseData();
    }

    /**
     * @throws GoCRMHttpResponseException
     */
    private function parseData() {
        try {
            $data = json_decode($this->rawData, true);
        } catch (\Exception $e) {
            throw new GoCRMHttpResponseException(GoCRMHttpResponseException::ERROR_INVALID_RESPONSE);
        }

        try {
            $this->status = $data['status'];
            $this->message = $data['message'];
            $this->data = $data['data'];
            $this->validationErrors = $data['validation_errors']??[];
            $this->pagination = $data['pagination']??[];
        } catch (\Exception $e) {
            throw new GoCRMHttpResponseException(GoCRMHttpResponseException::ERROR_INVALID_SCHEMA);
        }
    }

    /**
     * @return array
     */
    public function data(): array
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $attribute
     * @return mixed|null
     */
    public function __get(string $attribute)
    {
        return $this->data[$attribute]??null;
    }
}
