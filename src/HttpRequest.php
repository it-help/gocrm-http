<?php


namespace GoCRM\API\Http;


use GoCRM\API\Http\Client\HttpClient;


class HttpRequest
{
    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @var string
     */
    private $path;


    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @param string $url
     * @return string
     */
    private function buildUri(string $url): string
    {
        return HttpClient::mergePath('/', $this->path, $url);
    }

    /**
     * @param array|null $options
     * @return array
     */
    private function buildOptions(?array $options = []): array
    {
        $_options['headers'] = $options['headers']??[];
        $_options['params'] = $options['params']??[];
        $_options['body'] = $options['body']??[];

        return $_options;
    }

    /**
     * @param string $url
     * @param array $params
     * @return HttpResponse
     * @throws Exceptions\GoCRMHttpResponseException
     */
    public function get(string $url, array $params = []): HttpResponse
    {
        return $this->httpClient->send('GET', $this->buildUri($url), $this->buildOptions(['params' => $params]));
    }

    /**
     * @param string $url
     * @param array $params
     * @param array|null $body
     * @return HttpResponse
     * @throws Exceptions\GoCRMHttpResponseException
     */
    public function put(string $url, array $params = [], ?array $body = []): HttpResponse
    {
        return $this->httpClient->send('PUT', $this->buildUri($url), $this->buildOptions([
            'params' => $params,
            'body' => $body
        ]));
    }

    /**
     * @param string $url
     * @param array $params
     * @param array|null $body
     * @return HttpResponse
     * @throws Exceptions\GoCRMHttpResponseException
     */
    public function post(string $url, array $params = [], ?array $body = []): HttpResponse
    {
        return $this->httpClient->send('POST', $this->buildUri($url), $this->buildOptions([
            'params' => $params,
            'body' => $body
        ]));
    }


}
